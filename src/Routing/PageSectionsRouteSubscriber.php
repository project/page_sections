<?php

namespace Drupal\page_sections\Routing;

use Drupal\page_manager\PageVariantInterface;
use Drupal\page_manager\Routing\PageManagerRoutes;
use Symfony\Component\Routing\RouteCollection;
use Drupal\page_manager\PageInterface;

/**
 * Cleans up page sections routes.
 */
class PageSectionsRouteSubscriber extends PageManagerRoutes {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->entityStorage->loadMultiple() as $page_id => $page) {
      // Skip disabled and empty pages.
      if (
        !$page->status() ||
        !$page->getVariants()
      ) {
        continue;
      }

      foreach ($page->getVariants() as $variant_id => $variant) {
        if ($variant->getVariantPluginId() != 'page_sections_variant') {
          continue;
        }

        $this->cleanupRouteFor($page, $variant, $collection);
      }
    }
  }

  /**
   * Removes the route for the given section variant from the route collection.
   *
   * @param \Drupal\page_manager\PageInterface $page
   *   A page-manager page entity.
   * @param \Drupal\page_manager\PageVariantInterface $variant
   *   A variant on the page entity.
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   A collection of known routes.
   */
  protected function cleanupRouteFor(PageInterface $page, PageVariantInterface $variant, RouteCollection $collection) {
    $page_id = $page->id();
    $variant_id = $variant->id();
    $route_name = "page_manager.page_view_${page_id}_${variant_id}";

    if ($collection->get($route_name)) {
      $collection->remove($route_name);
    }
  }

}
