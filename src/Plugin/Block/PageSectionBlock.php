<?php

namespace Drupal\page_sections\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a page section block type.
 *
 * @Block(
 *  id = "page_sections",
 *  admin_label = @Translation("Page section"),
 *  category = @Translation("Page sections"),
 *  deriver = "Drupal\page_sections\Plugin\Derivative\PageSection"
 * )
 */
class PageSectionBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Drupal account to use for checking for access to block.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The page manager page variant entity.
   *
   * @var \Drupal\page_manager\PageVariantInterface
   */
  protected $pageVariant;

  /**
   * Constructs a new BlockContentBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which view access should be checked.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, AccountInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $page_variant = $this->getEntity();
    if ($page_variant
      // PageVariant::access does not check page status (and it should do).
      && $page_variant->status()
      && $page_variant->getPage()->status()) {
      return $page_variant->access('view', $account, TRUE);
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return ($page_variant = $this->getEntity())
      ? $this->entityTypeManager->getViewBuilder($page_variant->getEntityTypeId())->view($page_variant)
      : [];
  }

  /**
   * Loads the page variant entity.
   *
   * @return \Drupal\page_manager\PageVariantInterface
   *   The page manager page variant entity.
   */
  protected function getEntity() {
    if (!isset($this->pageVariant)) {
      $id = $this->getDerivativeId();
      $this->pageVariant = $this->entityTypeManager->getStorage('page_variant')->load($id);
    }
    return $this->pageVariant;
  }

}
