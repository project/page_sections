<?php

namespace Drupal\page_sections\Plugin\DisplayVariant;

use Drupal\panels\Plugin\DisplayVariant\PanelsDisplayVariant;

/**
 * Display variant that contains blocks and is exposed as well as block.
 *
 * @DisplayVariant(
 *   id = "page_sections_variant",
 *   admin_label = @Translation("Page section")
 * )
 */
class PageSectionsDisplayVariant extends PanelsDisplayVariant {

}
