<?php

namespace Drupal\page_sections\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieves block plugin definitions for all custom blocks.
 */
class PageSection extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The page manager page variant storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $pageVariantStorage;

  /**
   * PageSections constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $page_variant_storage
   *   The page manager page variant storage.
   */
  public function __construct(EntityStorageInterface $page_variant_storage) {
    $this->pageVariantStorage = $page_variant_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    /** @var \Drupal\Core\Entity\EntityTypeManager $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type_manager->getStorage('page_variant')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Reset the discovered definitions.
    $this->derivatives = [];

    /** @var \Drupal\page_manager\PageVariantInterface $page_variant */
    foreach ($this->pageVariantStorage->loadByProperties(['variant' => 'page_sections_variant']) as $page_variant) {
      $this->derivatives[$page_variant->id()] = $base_plugin_definition;
      $this->derivatives[$page_variant->id()]['admin_label'] = $page_variant->label();
      $this->derivatives[$page_variant->id()]['config_dependencies']['config'] = [
        $page_variant->getConfigDependencyName(),
      ];
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
